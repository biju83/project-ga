# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Apps', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='relationcollection',
            name='relation',
            field=models.ForeignKey(default=1, to='Apps.Relationship'),
            preserve_default=False,
        ),
    ]
