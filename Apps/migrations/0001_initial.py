# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ContainerType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('container_name', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ContainerTypeInstance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('container_type', models.ForeignKey(to='Apps.ContainerType')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ExtraField',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=55, blank=True)),
                ('description', models.CharField(max_length=255, blank=True)),
                ('value_field_choices', models.CharField(default=1, max_length=55, choices=[('INT', 'integerfield'), ('CHAR', 'CharField'), ('DAT', 'datefield')])),
                ('container_type', models.ForeignKey(to='Apps.ContainerType')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ExtraFieldInstance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('integer_value', models.IntegerField(null=True, blank=True)),
                ('char_value', models.CharField(max_length=255, null=True, blank=True)),
                ('date_value', models.DateField(null=True, blank=True)),
                ('container_type_instance', models.ForeignKey(to='Apps.ContainerTypeInstance')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Organisation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('org_name', models.CharField(max_length=255, blank=True)),
                ('location', models.CharField(max_length=255, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='RelationCollection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('child_cti', models.ManyToManyField(related_name=' relation.child_cti+', to='Apps.ContainerTypeInstance')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Relationship',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('relation_type', models.SmallIntegerField(default=1, choices=[(1, 'one to one'), (2, 'one to many'), (3, 'many to one'), (4, 'many to many')])),
                ('child_ct', models.ForeignKey(related_name='child_ct', to='Apps.ContainerType')),
                ('parent_ct', models.ForeignKey(related_name='parent_ct', to='Apps.ContainerType')),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('user_id', models.EmailField(max_length=254, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255, blank=True)),
                ('post_details', models.CharField(max_length=255, blank=True)),
                ('organisation', models.ForeignKey(to='Apps.Organisation')),
            ],
        ),
        migrations.AddField(
            model_name='relationcollection',
            name='created_by',
            field=models.ForeignKey(related_name='apps_relationcollection_created', blank=True, to='Apps.User', null=True),
        ),
        migrations.AddField(
            model_name='relationcollection',
            name='parent_cti',
            field=models.ForeignKey(related_name='relation.parent_cti+', to='Apps.ContainerTypeInstance'),
        ),
        migrations.AddField(
            model_name='relationcollection',
            name='updated_by',
            field=models.ForeignKey(related_name='apps_relationcollection_updated', blank=True, to='Apps.User', null=True),
        ),
        migrations.AddField(
            model_name='extrafieldinstance',
            name='created_by',
            field=models.ForeignKey(related_name='apps_extrafieldinstance_created', blank=True, to='Apps.User', null=True),
        ),
        migrations.AddField(
            model_name='extrafieldinstance',
            name='extra_field',
            field=models.ForeignKey(to='Apps.ExtraField'),
        ),
        migrations.AddField(
            model_name='extrafieldinstance',
            name='updated_by',
            field=models.ForeignKey(related_name='apps_extrafieldinstance_updated', blank=True, to='Apps.User', null=True),
        ),
        migrations.AddField(
            model_name='extrafield',
            name='created_by',
            field=models.ForeignKey(related_name='apps_extrafield_created', blank=True, to='Apps.User', null=True),
        ),
        migrations.AddField(
            model_name='extrafield',
            name='updated_by',
            field=models.ForeignKey(related_name='apps_extrafield_updated', blank=True, to='Apps.User', null=True),
        ),
        migrations.AddField(
            model_name='containertypeinstance',
            name='created_by',
            field=models.ForeignKey(related_name='apps_containertypeinstance_created', blank=True, to='Apps.User', null=True),
        ),
        migrations.AddField(
            model_name='containertypeinstance',
            name='updated_by',
            field=models.ForeignKey(related_name='apps_containertypeinstance_updated', blank=True, to='Apps.User', null=True),
        ),
        migrations.AddField(
            model_name='containertype',
            name='created_by',
            field=models.ForeignKey(related_name='apps_containertype_created', blank=True, to='Apps.User', null=True),
        ),
        migrations.AddField(
            model_name='containertype',
            name='organisation',
            field=models.ForeignKey(to='Apps.Organisation'),
        ),
        migrations.AddField(
            model_name='containertype',
            name='updated_by',
            field=models.ForeignKey(related_name='apps_containertype_updated', blank=True, to='Apps.User', null=True),
        ),
    ]
