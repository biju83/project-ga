# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from Apps.models import (Organisation,User,ContainerType,ExtraField,ContainerTypeInstance,ExtraFieldInstance,Relationship,RelationCollection)

# Register your models here.
admin.site.register(Organisation)
admin.site.register(User)
admin.site.register(ContainerType)
admin.site.register(ExtraField)
admin.site.register(ContainerTypeInstance)
admin.site.register(ExtraFieldInstance)
admin.site.register(Relationship)
admin.site.register(RelationCollection)
