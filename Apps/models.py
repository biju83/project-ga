# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
import json

# Create your models here

class Organisation(models.Model):
    #orgId=models.AutoField(primary_key=True)
    org_name=models.CharField(max_length=255, blank=True)
    location=models.CharField(max_length=255,  blank=True)

    def __str__(self):
        return str(self.org_name)

class User(models.Model):
    user_id=models.EmailField(primary_key=True)
    organisation=models.ForeignKey(Organisation)
    name=models.CharField(max_length=255,  blank=True)
    post_details=models.CharField(max_length=255,  blank=True)

    def __str__(self):
        return str(self.name)

class BaseModel(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, related_name="%(app_label)s_%(class)s_created",blank=True,null=True)
    updated_by = models.ForeignKey(User, related_name="%(app_label)s_%(class)s_updated",blank=True,null=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        abstract=True
 
class ContainerType(BaseModel):
        container_name=models.CharField(max_length=255,blank=False)
        organisation=models.ForeignKey(Organisation)


        def __str__(self):
            return str(self.container_name)

class ExtraField(BaseModel):
    INT=0
    CHAR=1
    #JSON=3
    DAT=4
    VALUE_FIELD_TYPE = (
        ('INT', 'integerfield'),
        ('CHAR', 'CharField'),
        #('JSON', 'jsonfield'),
        ('DAT', 'datefield'),
    )
    name=models.CharField(max_length=55,blank=True)
    description=models.CharField(max_length=255,blank=True)
    container_type=models.ForeignKey(ContainerType)
    value_field_choices=models.CharField(max_length=55, choices=VALUE_FIELD_TYPE,default=1)

    def __str__(self):
        return str(self.name)

class ContainerTypeInstance(BaseModel):
    container_type=models.ForeignKey(ContainerType)
    



    def __str__(self):
        return '%s %s' %(self.pk, self.container_type.container_name)        

class ExtraFieldInstance(BaseModel):
    extra_field=models.ForeignKey(ExtraField)
    container_type_instance=models.ForeignKey(ContainerTypeInstance)
    integer_value=models.IntegerField(null=True,blank=True)
    char_value=models.CharField(max_length=255,null=True,blank=True)
    date_value=models.DateField(null=True,blank=True) 

    def __str__(self):
          return str(self.extra_field.name)      



class Relationship(models.Model):
     
    OTO = 1
    OTM = 2
    MTO = 3
    MTM = 4

    RELATIONSHIP_CHOICES = (
    (OTO , 'one to one'),
    (OTM , 'one to many' ),
    (MTO , 'many to one'),
    (MTM , 'many to many'),
    )
    relation_type = models.SmallIntegerField(choices= RELATIONSHIP_CHOICES,default=OTO )
    parent_ct = models.ForeignKey(ContainerType, related_name = 'parent_ct')
    child_ct = models.ForeignKey(ContainerType, related_name='child_ct')

    def __str__(self):
        return '%s %s' %(self.parent_ct.container_name, self.child_ct.container_name) 

class RelationCollection(BaseModel):
    parent_cti = models.ForeignKey(ContainerTypeInstance ,related_name = 'relation.parent_cti+' )
    child_cti = models.ManyToManyField(ContainerTypeInstance ,related_name = ' relation.child_cti+')
    relation = models.ForeignKey(Relationship)
    
    def __str__(self):
          return str(self.parent_cti)
