# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core import serializers


from django.views.generic import View
from datetime import date, datetime
from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse,QueryDict
from django.core.exceptions import ObjectDoesNotExist
from  Apps.models import (User,Organisation,BaseModel,
						ContainerType,ExtraField,
						ContainerTypeInstance,
						ExtraFieldInstance,Relationship,
						RelationCollection)
import json
from django.db import connection 



class ContainerTypes(View):
	def get(self,request):
		try: 
			result=[]
			CT=ContainerType.objects.all()
			for ct in CT:
				result.append({
				'id' :  ct.id, 
				'container_name' :  ct.container_name,
				
				})
			print len(connection.queries)
			return HttpResponse(json.dumps(result), content_type='application/json',status=200) 
		except Exception as e:
			return HttpResponse("Internal server error",status=500)     


	def post(self,request):
		try:
			container_name = request.POST['container_name']
			created_by = request.POST['created_by']
			organisation = request.POST['organisation']
			user_data = User.objects.get(name__iexact = created_by)
			print(user_data)
			org = Organisation.objects.get(org_name__iexact = organisation)
			print(org)
			data = ContainerType(container_name=container_name,created_by_id=user_data.pk,organisation_id=org.pk)
			data.save()

			return HttpResponse("container_type created",status=201)  
		except ObjectDoesNotExist:
			return HttpResponse("user is not valid")

class ModifyCT(View):
	def put(self,request,ct_id):
		try:

			obj=QueryDict(request.body)
			obj_ct=ContainerType.objects.get(pk=ct_id)

			if obj_ct :
				obj_ct.container_name=obj.get('container_name')
				obj_ct.save()
				return HttpResponse("successful")

		except ObjectDoesNotExist:
			return HttpResponse("Record Not Found")


	def delete(self,request,ct_id):
		try:

			obj=QueryDict(request.body)
			obj_ct=ContainerType.objects.get(pk=ct_id)

			if obj_ct :
				obj_ct.is_deleted= True
				obj_ct.save()
				return HttpResponse("delete successful")

		except ObjectDoesNotExist:
			return HttpResponse("Record Not Found")        

		
class ExtraFields(View):
	def get(self,request,ct_id):
		try:
			#print len(connection.queries)
			obj=ExtraField.objects.filter(container_type__id=ct_id,is_deleted=False).select_related('container_type','created_by')
			result=[]
			for i in obj:
				print len(connection.queries)
				result.append({
				'id' :  i.id,   
				'name' :  i.name,
				'description' :  i.description,
				'container_type' :  i.container_type.container_name,
				'created_by' :  i.created_by.name,
				'created_on' :  str(i.created_on),
				'value_field_choices' :  i.value_field_choices,
				})
			#print len(connection.queries)
			return HttpResponse(json.dumps(result),content_type='application/json',status=200)
		except exceptions as e:
			return HttpResponse("enternal server error")

	def post(self,request,ct_id):
		try:
			name=request.POST['name']
			#description=request.POST['description']
			data_type=request.POST['data_type']
			created_by=request.POST['created_by']

			ct=ContainerType.objects.get(id=ct_id)
			created=User.objects.get(name=created_by)
			

			data=ExtraField(name=name,container_type_id=ct.pk,value_field_choices=data_type,created_by_id=created.pk)
			data.save()
			return HttpResponse("extrafield created successfully")
		except ObjectDoesNotExist:
			return HttpResponse("record not found")


class ModifyExtraFields(View):
	def put(self,request,ef_id):
		try:

			obj=QueryDict(request.body)
			obj_ef=ExtraField.objects.get(id = ef_id)

			if obj_ef :
				obj_ef.name=obj.get('name')
				obj_ef.value_field_choices = obj.get('data_type')
				obj_ef.save()
				return HttpResponse("successful")

		except ObjectDoesNotExist:
			return HttpResponse("Record Not Found")


	def delete(self,request,ef_id):
		try:

			obj_ef=ExtraField.objects.get(id=ef_id)

			if obj_ef :
				obj_ef.is_deleted= True
				obj_ef.save()
				return HttpResponse("delete successful")

		except ObjectDoesNotExist:
			return HttpResponse("Record Not Found")          



class ContainerTypeInstances(View):
	def get(self,request,ct_id):
   
		obj=ContainerTypeInstance.objects.filter(container_type__id__iexact=ct_id).select_related("container_type")
		result=[]
		
		
		for i in obj:
			data=[]
			res={}
			
			res['container_type'] = i.container_type.container_name
			res['ct_id'] = i.id
			

			ef_value_list=ExtraFieldInstance.objects.filter(container_type_instance__id=i.pk).select_related("extra_field","container_type_instance")
			
			for j in ef_value_list:
				
				res1 = {}
				
				if j.extra_field.value_field_choices=='INT':
				  
					res1['ef_id'] = j.extra_field.id
					res1['ef_name'] = j.extra_field.name
					res1['ef_value'] = j.integer_value
					res1['ef_value_id'] = j.id
					data.append(res1)
				
				if j.extra_field.value_field_choices=='CHAR':
				  
					res1['ef_id'] = j.extra_field.id
					res1['ef_name'] = j.extra_field.name
					res1['ef_value'] = j.char_value
					res1['ef_value_id'] = j.id
					data.append(res1)
													
				if j.extra_field.value_field_choices=='DAT':
				  
					res1['ef_id'] = j.extra_field.id
					res1['ef_name'] = j.extra_field.name
					res1['ef_value'] = str(j.date_value)
					res1['ef_value_id'] = j.id
					data.append(res1)
			
			res['data'] = data   
				
			result.append(res)
			print len(connection.queries)
		return HttpResponse(json.dumps(result), content_type='application/json')
								   


	def post(self,request,ct_id):
		
		ef_object=ExtraField.objects.filter(container_type_id=ct_id)
		ct_instance_data=ContainerTypeInstance(container_type_id=ct_id)
		ct_instance_data.save()
		data=QueryDict(request.body)
		for i in ef_object:
			
			if data[i.name] and i.value_field_choices=='CHAR':
				temp=ExtraFieldInstance(char_value=data[i.name],extra_field_id=i.pk,container_type_instance_id=ct_instance_data.pk)
				temp.save()
				

			elif data[i.name] and i.value_field_choices=='INT':
				temp=ExtraFieldInstance(integer_value=data[i.name],extra_field_id=i.pk,container_type_instance_id=ct_instance_data.pk)
				temp.save()
				

			elif data[i.name] and i.value_field_choices=='DAT':
				temp=ExtraFieldInstance(date_value=data[i.name],extra_field_id=i.pk,container_type_instance_id=ct_instance_data.pk)
				temp.save()
				

			else:
				return HttpResponse("invalid data")
		return HttpResponse("data added")

class ModifyCTInstance(View):
	
	def put(self,request,ci_id):
		try:
			#import pdb
			#pdb.set_trace()

			ci_object = ContainerTypeInstance.objects.get(pk=ci_id)
			post_data = QueryDict(request.body)
			ef_values = ExtraFieldInstance.objects.filter(container_type_instance_id=ci_id)
			for ef_value in ef_values:

				if post_data[ef_value.extra_field.name] and ef_value.extra_field.value_field_choices=='CHAR':
					ef_value.char_value=post_data[ef_value.extra_field.name]
					ef_value.save()

				elif post_data[ef_value.extra_field.name] and ef_value.extra_field.value_field_choices=='INT':
					ef_value.int_value=post_data[ef_value.extra_field.name]
					ef_value.save()

				elif post_data[ef_value.extra_field.name] and ef_value.extra_field.value_field_choices=='DAT':
					ef_vaue.date_value=post_data[ef_value.extra_field.name]
					ef_value.save()
					
				else:
					return HttpResponse("invalid_data")
				#pdb.set_trace()
			
			return HttpResponse("Success")

		except ObjectDoesNotExist:

			return HttpResponse("ObjectDoesNotExist")



	def delete(self,request,ci_id):
		try:

			obj_cti=ContainerTypeInstance.objects.get(id=ci_id)

			if obj_cti :
				obj_cti.is_deleted= True
				obj_cti.save()
				return HttpResponse("delete successful")

		except ObjectDoesNotExist:
			return HttpResponse("Record Not Found") 



class RelationshipView(View):
	def post(self,request):
		try :
			parent_id = request.POST['parent_id']
			child_id = request.POST['child_id']
			bool_exclusive = request.POST['bool_exclusive']
			bool_multiple = request.POST['bool_multiple']	

			pct_obj = ContainerType.objects.get(id = parent_id)
			cct_obj = ContainerType.objects.get(id = child_id)

			obj=Relationship.objects.filter(parent_ct_id=parent_id ,child_ct_id=child_id)
			

			if  not obj :
				if bool_exclusive == 'True' and bool_multiple == 'True':
					data = Relationship(parent_ct_id = pct_obj.pk , child_ct_id = cct_obj.pk , relation_type = Relationship.MTM )
					data.save()

				elif bool_exclusive == 'True' and bool_multiple == 'False' :
					data = Relationship(parent_ct_id = pct_obj.pk , child_ct_id = cct_obj.pk , relation_type = Relationship.OTM  )
					data.save()

				elif bool_exclusive == 'False' and bool_multiple == 'True' :
					data = Relationship(parent_ct_id = pct_obj.pk , child_ct_id = cct_obj.pk , relation_type = Relationship.MTO )
					data.save()

				elif bool_exclusive == 'False' and bool_multiple == 'False' :
					data = Relationship(parent_ct_id = pct_obj.pk , child_ct_id = cct_obj.pk , relation_type = Relationship.OTO )		
					data.save()
				else : 
					return HttpResponse(" provide correct input")
				return HttpResponse("Relation Established Successfully ",status=200)
			else :
				return HttpResponse("relation allready found")
		except ObjectDoesNotExist :
			return HttpResponse(" record not found ")



	def put(self,request):
		try :
			put_data = QueryDict(request.body)
			obj=Relationship.objects.get(parent_ct_id = put_data['parent_id'] , child_ct_id =put_data['child_id'])
			
			if put_data['bool_exclusive'] == 'True' and put_data['bool_multiple'] == 'True':
				if obj.relation_type != str(Relationship.MTM) :
					obj.relation_type = Relationship.MTM
					obj.save()
					return HttpResponse("Relation updated Successfully ",status=200)
				else :
					return 	HttpResponse("relation can't be updated")
				

			elif put_data['bool_exclusive'] == 'True' and put_data['bool_multiple'] == 'False':
				
				if obj.relation_type == str(Relationship.OTO) :
					obj.relation_type = Relationship.OTM
					obj.save()
					return HttpResponse("Relation updated Successfully ",status=200)
				else :
					return HttpResponse("Relation can't be updated ",status=201)

			elif put_data['bool_exclusive'] == 'False' and put_data['bool_multiple'] == 'True':
				if obj.relation_type == str(Relationship.OTO) :
					obj.relation_type = Relationship.MTO
					obj.save()
					return HttpResponse("Relation updated Successfully ",status=200)
				else :
					return HttpResponse("Relation can't be updated ",status=201)

			elif put_data['bool_exclusive'] == 'False' and put_data['bool_multiple'] == 'False'  :
				
				return HttpResponse("Relation can't be updated ",status=200)
				 
			else : 
				return HttpResponse(" provide correct input")
				

		except ObjectDoesNotExist :
			return HttpResponse(" record not found ")

class RelationPermission(View):
	
	def put(self,request,ci_id):
		try:
			put_data = QueryDict(request.body)
			
			parent_obj  = ContainerTypeInstance.objects.get(id = ci_id)
			
			pr_id = parent_obj.container_type.id
			
			child_obj = ContainerTypeInstance.objects.get(id = put_data['child_id'])
			ch_id = child_obj.container_type.id
			#print(child_id)
			rel_object = Relationship.objects.get(parent_ct_id = pr_id , child_ct_id = ch_id)
			if rel_object:
				print(rel_object.relation_type)
				return HttpResponse('allow')
		except ObjectDoesNotExist:
		 	return HttpResponse('deny')
	
	def post(self, request, ci_id) :
		try:
			params = request.POST
			child_id = params['child_id'].split()
			length = len(child_id)
			parent_ct_obj  = ContainerTypeInstance.objects.get(id=ci_id)
			
		
			for index in range(length):
				
				child_ct_obj = ContainerTypeInstance.objects.get(id=child_id[index])
				rel_object = Relationship.objects.get(parent_ct_id = parent_ct_obj.container_type.id , child_ct_id = child_ct_obj.container_type.id)
				print(rel_object.id)
				if rel_object.relation_type==str(Relationship.MTM) :
					rel_col = RelationCollection.objects.filter(parent_cti_id=ci_id  )
					print(rel_object.id)
					if rel_col:
						
						for child in rel_col[0].child_cti.all():
							
							if child_id[index]!=child.pk:
								print('hello1')
								rel_col[0].child_cti.add(child_ct_obj)
					else :
						
						add_relation = RelationCollection(parent_cti_id=ci_id ,relation_id = rel_object.id)
						add_relation.save()	
						add_relation.child_cti.add(child_ct_obj)

				elif rel_object.relation_type==str(Relationship.OTM) :
					rel_col = RelationCollection.objects.filter(parent_cti_id=ci_id)
					if rel_col:

						for ch in RelationCollection.objects.all():
							for child in ch.child_cti.all() :
								if child_id[index]!=child.pk  :
									rel_col[0].child_cti.add(child_ct_obj) 

					else :
						
						for ch in RelationCollection.objects.all():
							for child in ch.child_cti.all() :
								if child_id[index]!=child.pk :
									add_relation = RelationCollection(parent_cti_id=ci_id )
									add_relation.save()
									add_relation.child_cti.add(child_ct_obj) 

						
						

				elif rel_object.relation_type==str(Relationship.OTO) :
					rel_col = RelationCollection.objects.filter(parent_cti_id=ci_id )
					if not rel_col :
						for ch in RelationCollection.objects.all():
							for child in ch.child_cti.all() :
								if child_id[index]!=child.pk :
									add_relation = RelationCollection(parent_cti_id=ci_id )
									add_relation.save()
									add_relation.child_cti.add(child_ct_obj)
					
			return HttpResponse("record successfully created")
						

		except ObjectDoesNotExist:
			return HttpResponse ('record not presesnt')














			
		

		
























			

