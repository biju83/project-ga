
from django.conf.urls import url
from django.contrib import admin
#from Apps.views import OrganisationDetails,UserDetails,ContainerTypes,ExtraFields,ContainerTypeValues,ExtraFieldValues,GetUserPersonalDetails
from Apps.views import (ContainerTypes,
                        ModifyCT,
                        ExtraFields,
                        ModifyExtraFields,
                        ContainerTypeInstances,
                        ModifyCTInstance,
                        RelationshipView,
                        RelationPermission)
urlpatterns = [

    url(r'^admin/', admin.site.urls),
         
    url(r'^container-types/',ContainerTypes.as_view(),name='container_types'),
    url(r'^ct-types/(?P<ct_id>[0-9]+)/',ModifyCT.as_view(),name="modify CT "),

    url(r'^extra-fields/container-types/(?P<ct_id>[0-9]+)/',ExtraFields.as_view(),name="extra-fields"),
    url(r'^extra-fields/(?P<ef_id>[0-9]+)/',ModifyExtraFields.as_view(),name="modify EF"),
    
    url(r'^container-instances/container-types/(?P<ct_id>[0-9]+)/',ContainerTypeInstances.as_view(),name="CT-instances"),
    url(r'^ct-instances/(?P<ci_id>[0-9]+)/',ModifyCTInstance.as_view(),name="modify ct-instances"),

    url(r'^ct-types/relations/',RelationshipView.as_view(),name='relation post and update'),
    url(r'^container-instances/(?P<ci_id>[0-9]+)/', RelationPermission.as_view(),name="relation"),
    
    
    






]
